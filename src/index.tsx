import * as React from 'react';

import '../css/styles.css';

import * as PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 * Shared notifications for easy access.
 */
let shared: Notification[] = []

interface Props {
  /**
   * Whether to make this notification accessible from anywhere.
   */
  shared?: boolean;
}

class State {
  message: string = '';
  visible: boolean = false;
}

/**
 * Displays a small, important message.
 * @author Johan Svensson
 */
export default class Notification extends React.Component<Props, State> {
  state = new State();

  static propTypes = {
    shared: PropTypes.bool
  }

  /**
   * @returns All shared and mounted notification instances.
   */
  static get shared() {
    return shared;
  }

  /**
   * Shows a message with the first shared component, if one exists.
   */
  static showWithFirstShared(message: string) {
    let instance = shared[0];
    if (instance) {
      instance.show(message);
    }
  }


  animationDuration = 250;
  hideTimeoutId = -1;

  componentDidMount(){
    if (this.props.shared) {
      shared.push(this);
    }
  }

  componentWillUnmount(){
    shared = shared.filter(n => n != this);
  }

  /**
   * Shows a notification message.
   */
  show(message: string, duration: number = 3500) {
    let { state } = this;
    if (state.visible) {
      //  Hide then show
      clearTimeout(this.hideTimeoutId);
      this.hide();
      setTimeout(() => this.show(message), this.animationDuration + 50);
      return;
    }

    this.setState({
      message,
      visible: true
    });

    clearTimeout(this.hideTimeoutId);
    this.hideTimeoutId = setTimeout(() => this.hide(), duration);
  }

  hide() {
    this.setState({
      visible: false
    });
  }

  render() {
    let { state, props } = this;

    return (
      <div className={classNames({
        notification: true,
        visible: state.visible
      })} style={{ transitionDuration: `${this.animationDuration}ms` }}>
        <p className="text">{state.message}</p>
      </div>
    )
  }
}