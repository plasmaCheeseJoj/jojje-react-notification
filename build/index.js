"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
require("../css/styles.css");
var PropTypes = __importStar(require("prop-types"));
var classnames_1 = __importDefault(require("classnames"));
/**
 * Shared notifications for easy access.
 */
var shared = [];
var State = /** @class */ (function () {
    function State() {
        this.message = '';
        this.visible = false;
    }
    return State;
}());
/**
 * Displays a small, important message.
 * @author Johan Svensson
 */
var Notification = /** @class */ (function (_super) {
    __extends(Notification, _super);
    function Notification() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = new State();
        _this.animationDuration = 250;
        _this.hideTimeoutId = -1;
        return _this;
    }
    Object.defineProperty(Notification, "shared", {
        /**
         * @returns All shared and mounted notification instances.
         */
        get: function () {
            return shared;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Shows a message with the first shared component, if one exists.
     */
    Notification.showWithFirstShared = function (message) {
        var instance = shared[0];
        if (instance) {
            instance.show(message);
        }
    };
    Notification.prototype.componentDidMount = function () {
        if (this.props.shared) {
            shared.push(this);
        }
    };
    Notification.prototype.componentWillUnmount = function () {
        var _this = this;
        shared = shared.filter(function (n) { return n != _this; });
    };
    /**
     * Shows a notification message.
     */
    Notification.prototype.show = function (message, duration) {
        var _this = this;
        if (duration === void 0) { duration = 3500; }
        var state = this.state;
        if (state.visible) {
            //  Hide then show
            clearTimeout(this.hideTimeoutId);
            this.hide();
            setTimeout(function () { return _this.show(message); }, this.animationDuration + 50);
            return;
        }
        this.setState({
            message: message,
            visible: true
        });
        clearTimeout(this.hideTimeoutId);
        this.hideTimeoutId = setTimeout(function () { return _this.hide(); }, duration);
    };
    Notification.prototype.hide = function () {
        this.setState({
            visible: false
        });
    };
    Notification.prototype.render = function () {
        var _a = this, state = _a.state, props = _a.props;
        return (React.createElement("div", { className: classnames_1.default({
                notification: true,
                visible: state.visible
            }), style: { transitionDuration: this.animationDuration + "ms" } },
            React.createElement("p", { className: "text" }, state.message)));
    };
    Notification.propTypes = {
        shared: PropTypes.bool
    };
    return Notification;
}(React.Component));
exports.default = Notification;
